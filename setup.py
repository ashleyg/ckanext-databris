from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
	name='ckanext-databris',
	version=version,
	description="CKAN extensions for data.bris",
	long_description="""\
	""",
	classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
	keywords='',
	author='Jasper Tredgold',
	author_email='jasper.tredgold@bris.ac.uk',
	url='https://bitbucket.org/ecjet/ckanext-databris',
	license='GPL',
	packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
	namespace_packages=['ckanext', 'ckanext.databris'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[
		# -*- Extra requirements: -*-
	],
	entry_points=\
	"""
        [ckan.plugins]
		# Add plugins here, eg
		# myplugin=ckanext.databris:PluginClass
		ckan_databris_harvester=ckanext.databris.harvesters.databris:DataBrisHarvester
		ckan_databris_theme=ckanext.databris.theme:DataBrisTheme
	""",
)
