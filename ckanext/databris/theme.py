import os

from logging import getLogger

from ckan.plugins import implements, SingletonPlugin
from ckan.plugins import IConfigurer, IDatasetForm
from ckan.lib.plugins import DefaultDatasetForm

from ckan.authz import Authorizer

from ckan.lib.base import c, model

from ckan.logic import get_action, NotFound
from ckan.logic.schema import package_form_schema

from ckan.logic.converters import convert_to_extras, convert_from_extras, convert_to_tags, convert_from_tags, free_tags_only
from ckan.lib.navl.validators import ignore_missing, keep_extras, not_empty

log = getLogger(__name__)
from pprint import pprint

GENRE_VOCAB = u'genre_vocab'
COMPOSER_VOCAB = u'composer_vocab'

class DataBrisTheme(SingletonPlugin, DefaultDatasetForm):
    implements(IConfigurer, inherit=True)
    implements(IDatasetForm, inherit=True)

    def update_config(self, config):
        here = os.path.dirname(__file__)
        rootdir = os.path.dirname(os.path.dirname(here))
        template_dir = os.path.join(rootdir, 'ckanext', 'databris', 'theme', 'templates')
        config['extra_template_paths'] = ','.join([template_dir, config.get('extra_template_paths', '')])

    def package_types(self):
        log.debug("HERE - package types")
        return ['dataset']

    def form_to_db_schema(self):
        """
        Returns the schema for mapping package data from a form to a format
        suitable for the database.
        """
        schema = package_form_schema()
        schema.update({
            'authors': {
                'url': [ignore_missing, unicode],
                'name': [not_empty, unicode]
            }
        })
        log.debug("HERE - form to db")
        log.debug(schema)
        return schema

    def db_to_form_schema(self):
        """
        Returns the schema for mapping package data from the database into a
        format suitable for the form (optional)
        """
        schema = package_form_schema()
        schema.update({
            'authors': {
                'url': [ignore_missing, unicode],
                'name': [not_empty, unicode]
            }
        })
        log.debug("HERE - db to form")
        log.debug(schema)
        return schema

    def check_data_dict(self, data_dict):
        """
        Check if the return data is correct and raises a DataError if not.
        """
        return
