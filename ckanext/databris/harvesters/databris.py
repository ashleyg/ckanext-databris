#coding: utf-8
import urllib2

from HTMLParser import HTMLParser 
import re

from wsgiref.handlers import format_date_time
from time import mktime

from rdflib.graph import Graph
from rdflib import Namespace, RDF, URIRef, RDFS

from types import NoneType

from ckan.lib.base import c
from ckan import model
from ckan.model import Session, Package
from ckan.logic import ValidationError, NotFound, get_action
from ckan.lib.helpers import json

from ckanext.harvest.model import HarvestJob, HarvestObject, HarvestGatherError, \
                                    HarvestObjectError

from ckanclient import CkanClient

import logging
log = logging.getLogger(__name__)
from pprint import pprint

from ckanext.harvest.harvesters.base import HarvesterBase

# create a parser
class ListingHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.ids = []

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for name, value in attrs:
                # directory pattern: alphanumeric, min length 25, ends with '/'
                if name == 'href' and re.match('[a-z0-9]{25,}\/',value):
                    self.ids.append(value.rstrip('/'))

    def get_ids(self):
        return self.ids

class DataBrisHarvester(HarvesterBase):
    '''
    A Harvester for data.bris
    '''
    config = None

    api_version = '2'

    def __init__(self):
        self.repo_default = 'http://54.246.101.109:81/datasets/'

    def _get_content(self, url):
        http_request = urllib2.Request(
            url = url,
        )

        try:
            api_key = self.config.get('api_key',None)
            if api_key:
                http_request.add_header('Authorization',api_key)
            http_response = urllib2.urlopen(http_request)

            return http_response.read()
        except Exception, e:
            raise e

    def _is_modified(self, url, date):
        http_request = urllib2.Request(
            url = url,
        )

        stamp = mktime(date.timetuple())
        http_date = format_date_time(stamp)

        try:
            api_key = self.config.get('api_key',None)
            if api_key:
                http_request.add_header('Authorization',api_key)
            http_request.add_header('If-Modified-Since', http_date)
            log.debug('Checking mod (%s) on %s', http_date, url)
            urllib2.urlopen(http_request)
        except urllib2.URLError, e:
            if e.code == 304:
                return False
            else:
                return True
        else:
            return True

    def _get_datasets(self, content):
        try:
            parser = ListingHTMLParser()
            parser.feed(content)
            return parser.get_ids()
        except Exception, e:
            raise e

    def _load_rdf(self, content):
        DCMI = Namespace("http://purl.org/dc/dcmitype/")
        DC = Namespace("http://purl.org/dc/terms/")
        package = {}
        try:
            g = Graph()
            # name and id
            # TODO update this when DS adds a 'slug' property to Collection
            g.parse(data=content, format='application/rdf+xml')
            # FIXME assumption to get top Collection
            uris = list(g.subjects(predicate=DC['issued']))
            uri = uris[0]
            uid = str(uri).lstrip('repo:') # ckan name only allows lowercase alphanumeric and _-
            package['name'] = uid
            package['id'] = uid
            # url 
            # TODO currently scheme is repo:, but will become resolvable http: url
            package['url'] = str(uri).replace('repo:',str(self.config.get('repo', self.repo_default)))
            # title
            literal = g.value(predicate=DC["title"], subject=uri)
            package['title'] = str(literal)
            # authors
            # TODO the default package schema does not support multiple authors
            # need to use a custom schema: https://ckan.readthedocs.org/en/ckan-1.8/forms.html#custom-schemas
            # licence
            # TODO there is also license_title and license_id - populated from controlled pick list?
            literal = g.value(predicate=DC["rights"], subject=uri)
            package['license'] = str(literal)
            # description
            literal = g.value(predicate=DC["description"], subject=uri)
            package['notes'] = str(literal)
            # tags
            # the tags list is munged in superclass
            literals = g.objects(predicate=DC["subject"], subject=uri)
            tags = []
            for literal in literals:
                tags.append(str(literal))
            package['tags'] = tags
            # authors
            uris = g.objects(predicate=DC["creator"], subject=uri)
            authors = []
            for uri in uris:
                author_dict = {}
                author_dict['url'] = str(uri)
                literal = g.value(predicate=RDFS["label"], subject=uri)
                author_dict['name'] = str(literal)
                authors.append(author_dict)
            package['authors'] = authors
            # resource
            uris = g.objects(predicate=DC["hasPart"])
            resources = []
            for uri in uris:
                # we don't want any folders
                if type(g.value(predicate=DC["hasPart"], subject=uri)) is NoneType:
                    resource_dict = {}
                    resource_dict['package_id'] = uid
                    resource_dict['type'] = 'file'
                    resource_dict['url'] = str(uri).replace('repo:',str(self.config.get('repo', self.repo_default)))
                    # FIXME hack to get path into name
                    resource_dict['name'] = str(uri).partition('/')[2]
                    literal = g.value(predicate=DC["format"], subject=uri)
                    resource_dict['format'] = str(literal)
                    resource_dict['mimetype'] = str(literal)
                    resources.append(resource_dict)
            package['resources'] = resources
            package['type'] = 'dataset'
            pprint(package)
        except Exception, e:
            raise e
        return package

    def _set_config(self,config_str):
        if config_str:
            self.config = json.loads(config_str)

            if 'api_version' in self.config:
                self.api_version = self.config['api_version']

            if 'repo' in self.config:
                self.repo = self.config['repo']

            log.debug('Using config: %r', self.config)
        else:
            self.config = {}

    def info(self):
        return {
            'name': 'databris',
            'title': 'data.bris harvester',
            'description': 'Harvester that can parse data.bris info files',
            'form_config_interface':'Text'
        }

    def validate_config(self,config):
        if not config:
            return config

        try:
            config_obj = json.loads(config)

            if 'default_tags' in config_obj:
                if not isinstance(config_obj['default_tags'],list):
                    raise ValueError('default_tags must be a list')

            if 'default_groups' in config_obj:
                if not isinstance(config_obj['default_groups'],list):
                    raise ValueError('default_groups must be a list')

                # Check if default groups exist
                context = {'model':model,'user':c.user}
                for group_name in config_obj['default_groups']:
                    try:
                        group = get_action('group_show')(context,{'id':group_name})
                    except NotFound,e:
                        raise ValueError('Default group not found')

            if 'default_extras' in config_obj:
                if not isinstance(config_obj['default_extras'],dict):
                    raise ValueError('default_extras must be a dictionary')

            if 'user' in config_obj:
                # Check if user exists
                context = {'model':model,'user':c.user}
                try:
                    user = get_action('user_show')(context,{'id':config_obj.get('user')})
                except NotFound,e:
                    raise ValueError('User not found')

            for key in ('read_only','force_all'):
                if key in config_obj:
                    if not isinstance(config_obj[key],bool):
                        raise ValueError('%s must be boolean' % key)

        except ValueError,e:
            raise e

        return config


    def gather_stage(self,harvest_job):
        log.debug('In DataBrisHarvester gather_stage (%s)' % harvest_job.source.url)
        get_all_packages = True
        package_ids = []

        self._set_config(harvest_job.source.config)

        # Check if this source has been harvested before
        previous_job = Session.query(HarvestJob) \
                        .filter(HarvestJob.source==harvest_job.source) \
                        .filter(HarvestJob.gather_finished!=None) \
                        .filter(HarvestJob.id!=harvest_job.id) \
                        .order_by(HarvestJob.gather_finished.desc()) \
                        .limit(1).first()

        # Get source URL
        base_url = harvest_job.source.url
        if not base_url.endswith('/'):
            base_url += '/'

        if (previous_job and not previous_job.gather_errors and not len(previous_job.objects) == 0):
            if not self.config.get('force_all',False):
                log.debug('Gathering modified datasets')
                get_all_packages = False

                # Request only the packages modified since last harvest job
                last_time = harvest_job.gather_started

                try:
                    content = self._get_content(base_url)
                    datasets = self._get_datasets(content)
                    for dataset in datasets:
                        dataset_url = base_url + dataset
                        if not dataset_url.endswith('/'):
                            dataset_url += '/'
                        try:
                            if self._is_modified(dataset_url + '.info.rdf', last_time):
                                package_ids.append(dataset)
                        except Exception, e:
                            # problem with checking is modified - add to list
                            self._save_gather_error('Problem checking last-mod for URL: %s: %s. Adding to list to be harvested' % (dataset_url, str(e)),harvest_job)
                            package_ids.append(dataset)
                    if not len(package_ids):
                        log.info('No data.bris datasets have been updated since the last harvest job')
                        return None
                except Exception, e:
                    self._save_gather_error('Unable to get content for URL: %s: %s. Attempting to get all datasets' % (base_url, str(e)),harvest_job)
                    get_all_packages = True

        if get_all_packages:
            # Request all remote packages
            log.debug('Gathering all datasets')
            try:
                content = self._get_content(base_url)
                package_ids = self._get_datasets(content)
            except Exception,e:
                self._save_gather_error('Unable to get content for URL: %s: %s' % (base_url, str(e)),harvest_job)
                return None

        log.debug('Got dataset ids: (%s)' % package_ids)

        try:
            object_ids = []
            if len(package_ids):
                for package_id in package_ids:
                    # Create a new HarvestObject for this identifier
                    obj = HarvestObject(guid = package_id, job = harvest_job)
                    obj.save()
                    object_ids.append(obj.id)

                return object_ids

            else:
               self._save_gather_error('No datasets received for URL: %s' % url,
                       harvest_job)
               return None
        except Exception, e:
            self._save_gather_error('%r'%e.message,harvest_job)


    def fetch_stage(self,harvest_object):
        log.debug('In DataBrisHarvester fetch_stage')

        self._set_config(harvest_object.job.source.config)

        # Get source URL
        url = harvest_object.source.url
        if not url.endswith('/'):
            url += '/'
        url = url + harvest_object.guid
        if not url.endswith('/'):
            url += '/'

        # Get contents
        try:
            content = self._get_content(url + '.info.rdf')
        except Exception,e:
            self._save_object_error('Unable to get content for package: %s: %r' % \
                                        (url, e),harvest_object)
            return None

        # Save the fetched contents in the HarvestObject
        harvest_object.content = content
        harvest_object.save()
        return True

    def import_stage(self,harvest_object):
        log.debug('In DataBrisHarvester import_stage')
        if not harvest_object:
            log.error('No harvest object received')
            return False

        if harvest_object.content is None:
            self._save_object_error('Empty content for object %s' % harvest_object.id,
                    harvest_object, 'Import')
            return False

        self._set_config(harvest_object.job.source.config)

        try:
            package_dict = self._load_rdf(harvest_object.content)

            # Set default tags if needed
            default_tags = self.config.get('default_tags',[])
            if default_tags:
                if not 'tags' in package_dict:
                    package_dict['tags'] = []
                package_dict['tags'].extend([t for t in default_tags if t not in package_dict['tags']])

            # Ignore remote groups for the time being
            if 'groups' in package_dict:
                del package_dict['groups']

            # Set default groups if needed
            default_groups = self.config.get('default_groups',[])
            if default_groups:
                if not 'groups' in package_dict:
                    package_dict['groups'] = []
                package_dict['groups'].extend([g for g in default_groups if g not in package_dict['groups']])

            # Set default extras if needed
            default_extras = self.config.get('default_extras',{})
            if default_extras:
                override_extras = self.config.get('override_extras',False)
                if not 'extras' in package_dict:
                    package_dict['extras'] = {}
                for key,value in default_extras.iteritems():
                    if not key in package_dict['extras'] or override_extras:
                        # Look for replacement strings
                        if isinstance(value,basestring):
                            value = value.format(harvest_source_id=harvest_object.job.source.id,
                                     harvest_source_url=harvest_object.job.source.url.strip('/'),
                                     harvest_source_title=harvest_object.job.source.title,
                                     harvest_job_id=harvest_object.job.id,
                                     harvest_object_id=harvest_object.id,
                                     dataset_id=package_dict['id'])

                        package_dict['extras'][key] = value

            result = self._create_or_update_package(package_dict,harvest_object)

            if result and self.config.get('read_only',False) == True:

                package = model.Package.get(package_dict['id'])

                # Clear default permissions
                model.clear_user_roles(package)

                # Setup harvest user as admin
                user_name = self.config.get('user',u'harvest')
                user = model.User.get(user_name)
                pkg_role = model.PackageRole(package=package, user=user, role=model.Role.ADMIN)

                # Other users can only read
                for user_name in (u'visitor',u'logged_in'):
                    user = model.User.get(user_name)
                    pkg_role = model.PackageRole(package=package, user=user, role=model.Role.READER)


        except ValidationError,e:
            self._save_object_error('Invalid package with GUID %s: %r' % (harvest_object.guid, e.error_dict),
                    harvest_object, 'Import')
        except Exception, e:
            self._save_object_error('%r'%e,harvest_object,'Import')
